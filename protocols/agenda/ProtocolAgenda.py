import spade
from RandomGenerator import GenerateGroupID
from CheckCollisions import CheckCollisions

class StartProtocolAgenda(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		performative = self.myAgent.performative
		if(performative == "inform"):				
			self.myAgent.addBehaviour(ExecuteAddEvent())
		elif(performative == "inform_if"):
			self.myAgent.addBehaviour(ExecuteModifyEvent())
		elif(performative == "inform_ref"):
			self.myAgent.addBehaviour(ExecuteRemoveEvent())
		elif(performative == "request"):
			self.myAgent.addBehaviour(ExecuteViewAgenda())
		elif(performative == "propose"):
			self.myAgent.addBehaviour(AutomaticFillingBehaviour())			
	
class ExecuteAddEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		try:
			# Open a file to writing events and create it if id does not exist
			f = open(self.myAgent.jid + ".txt","a+")
			name = str(self.myAgent.content.__getattr__("rdf:name")) 
			day = str(self.myAgent.content.__getattr__("rdf:day"))
			month = str(self.myAgent.content.__getattr__("rdf:month"))
			year = str(self.myAgent.content.__getattr__("rdf:year"))
			hour = str(self.myAgent.content.__getattr__("rdf:hour"))
			duration = str(self.myAgent.content.__getattr__("rdf:duration"))
			priority = str(self.myAgent.content.__getattr__("rdf:priority"))
			movable = str(self.myAgent.content.__getattr__("rdf:movable"))
			if movable == "true":
				movable = "MOVABLE"
			else:
				movable = "" 	
			event = str(self.myAgent.content.__getattr__("rdf:event"))

			f.write("Event: "+event+ " Hour: "+hour+" Day: "+day+" Month: "+month+" Year: "+year+\
				" Duration: "+duration+" Priority: "+priority+" "+movable+"\n")
			f.close()

			self.myAgent.user_name = name
			self.myAgent.user_event = event

			self.myAgent.addBehaviour(ResponseAddEventTRUE())		
		except:
			self.myAgent.addBehaviour(ResponseAddEventFALSE())			

class ResponseAddEventTRUE(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ResponseAddEventFALSE(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("failure")          
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		
		
class ExecuteViewAgenda(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		
		f = open(self.myAgent.jid + ".txt", "r+")
		content = f.read()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["agenda"] = content	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ExecuteRemoveEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		remove = int(self.myAgent.content.__getattr__("rdf:event"))	

		try:
			with open(self.myAgent.jid + ".txt", 'r+') as f:
				lines = f.readlines()
				f.seek(0)
				for i, line in enumerate(lines):
					if i != remove: 
						f.write(line)
				f.truncate()		
				f.close()	

			self.addBehaviour(ResponseRemoveEventTRUE())		

		except:
			self.addBehaviour(ResponseRemoveEventFALSE())						

class ResponseRemoveEventTRUE(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ResponseRemoveEventFALSE(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("failure")          
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ExecuteModifyEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		modify = int(self.myAgent.content.__getattr__("rdf:modify"))

		try:
			with open(self.myAgent.jid + ".txt", 'r+') as f:
				lines = f.readlines()
				f.seek(0)
				for i, line in enumerate(lines):
					if i != modify: 
						f.write(line)
					else: 
						day = str(self.myAgent.content.__getattr__("rdf:day"))
						month = str(self.myAgent.content.__getattr__("rdf:month"))
						year = str(self.myAgent.content.__getattr__("rdf:year"))
						hour = str(self.myAgent.content.__getattr__("rdf:hour"))
						duration = str(self.myAgent.content.__getattr__("rdf:duration"))
						priority = str(self.myAgent.content.__getattr__("rdf:priority"))
						movable = str(self.myAgent.content.__getattr__("rdf:movable"))
						if movable == "true":
							movable = "MOVABLE"
						else:
							movable = "" 	
						event = str(self.myAgent.content.__getattr__("rdf:event"))

						f.write("Event: "+event+ " Hour: "+hour+" Day: "+day+" Month: "+month+" Year: "+year+\
							" Duration: "+duration+" Priority: "+priority+" "+movable+"\n")

				f.truncate()		
				f.close()

				self.myAgent.addBehaviour(ResponseModifyEventTRUE())

		except:
				self.myAgent.addBehaviour(ResponseModifyEventFALSE())
				
class ResponseModifyEventTRUE(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ResponseModifyEventFALSE(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("failure")          
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class AutomaticFillingBehaviour(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		# When the event is successfully put on agenda, status is TRUE
		status = False
		# half_hour is the preferred hour plus one (not 30 minutes)
		half_hour = 0
		self.myAgent.hour = 0
		priority = [self.myAgent.hour_1, self.myAgent.hour_2, self.myAgent.hour_3, self.myAgent.hour_4, self.myAgent.hour_5]

		self.myAgent.event = str(self.myAgent.content.__getattr__("rdf:event"))
		self.myAgent.day = int(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = int(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = int(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.duration = int(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.priority = int(self.myAgent.content.__getattr__("rdf:priority"))

		# At first, try put the event in preferred hour position
		while len(priority) > 0:
			# max() get the most high value. If in the list have two or more equal high values, then max() get the high from
			#  left to right
			preferred = max(priority)			
			if preferred == self.myAgent.hour_1 and self.myAgent.hour < 8:
				self.myAgent.hour = 8 + half_hour
			elif preferred == self.myAgent.hour_2 and self.myAgent.hour < 10:
				self.myAgent.hour = 10 + half_hour 	
			elif preferred == self.myAgent.hour_3 and self.myAgent.hour < 12:
				self.myAgent.hour = 12 + half_hour	
			elif preferred == self.myAgent.hour_4 and self.myAgent.hour < 14:
				self.myAgent.hour = 14 + half_hour
			elif preferred == self.myAgent.hour_5 and self.myAgent.hour < 16:
				self.myAgent.hour = 16 + half_hour			
	
			obj = CheckCollisions()	
			collision = obj.check(self.myAgent.jid, self.myAgent.hour, self.myAgent.day,\
			 self.myAgent.month, self.myAgent.year, self.myAgent.duration)

			if collision == False:
				status = True
				break
			elif self.myAgent.duration > 1 or half_hour == 1:	
				priority.remove(preferred)
				half_hour = 0
			else:	
				half_hour = 1 	

		# Send a message to yourself triggering the ExecuteAddEvent()		
		if status == True:
			self.msg = spade.ACLMessage.ACLMessage()     
			self.msg.setPerformative("inform")           
			self.msg.setProtocol("Agenda")               

			receiver = spade.AID.aid(name= self.myAgent.jid, 
	                             addresses=["xmpp://" + self.myAgent.jid])
			self.msg.addReceiver(receiver)               

			self.data = spade.content.ContentObject()
			self.data["name"] = self.myAgent.jid
			self.data["day"] = str(self.myAgent.day)
			self.data["month"] = str(self.myAgent.month)
			self.data["year"] = str(self.myAgent.year)
			self.data["hour"] = str(self.myAgent.hour)
			self.data["duration"] = str(self.myAgent.duration)
			self.data["priority"] = str(self.myAgent.priority)	
			self.data["movable"] = "true"
			if self.myAgent.event != " ":
				self.data["event"] = self.myAgent.event	
			else:
				self.data["event"] = "Random Event"	

			self.msg.setContentObject(self.data)    

			self.myAgent.send(self.msg)		

		# If is not possible put the event in the agenda, trying shift others		
		else:
			self.myAgent.addBehaviour(AutomaticShiftBehaviour()) 	

class AutomaticShiftBehaviour(spade.Behaviour.OneShotBehaviour):			
	def _process(self):
		# At first, find the free slots
		# free_slot vector represent the hours, one by one from 8am to 6pm
		global free_slot
		free_slot = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		# movable vector represent the events can be moved
		movable = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		# priority vector represent the priority of events
		priority = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

		with open(self.myAgent.jid + ".txt", 'r+') as f:
			lines = f.readlines()
			print len(lines)
			for line in lines:
				hour = int(line.split(' ')[3])
				day = int(line.split(' ')[5])
				month = int(line.split(' ')[7])
				year = int(line.split(' ')[9])
				duration = int(line.split(' ')[11])
				priority_value = int(line.split(' ')[13])
				# Check the agenda compromisses event by event, and change free_slot vector
				if day == self.myAgent.day and month == self.myAgent.month and year == self.myAgent.year:
					if hour == 8:
						self.notAvailable(0, duration)
						if "MOVABLE" in line:
							movable[0] = duration
							priority[0] = priority_value
					elif hour == 9:
						self.notAvailable(1, duration)	
						if "MOVABLE" in line:
							movable[1] = duration 
							priority[1] = priority_value
					elif hour == 10:
						self.notAvailable(2, duration)		
						if "MOVABLE" in line:
							movable[2] = 1 
							priority[2] = priority_value
					elif hour == 11:
						self.notAvailable(3, duration)	
						if "MOVABLE" in line:
							movable[3] = duration
							priority[3] = priority_value
					elif hour == 12:
						self.notAvailable(4, duration)		
						if "MOVABLE" in line:
							movable[4] = duration
							priority[4] = priority_value
					elif hour == 13:
						self.notAvailable(5, duration)
						if "MOVABLE" in line:
							movable[5] = duration
							priority[5] = priority_value
					elif hour == 14:
						self.notAvailable(6, duration)		
						if "MOVABLE" in line:
							movable[6] = duration
							priority[6] = priority_value
					elif hour == 15:
						self.notAvailable(7, duration)		
						if "MOVABLE" in line:
							movable[7] = duration
							priority[7] = priority_value
					elif hour == 16:
						self.notAvailable(8, duration)		
						if "MOVABLE" in line:
							movable[8] = duration
							priority[8] = priority_value
					elif hour == 17:
						self.notAvailable(9, 1)		
						if "MOVABLE" in line:
							movable[9] = duration
							priority[9] = priority_value
			f.close()

		# Count ocurrences of 0s in free_slot vector. It necessary can be at least the duration of the
		#  event that user want be add, otherwhise, returns that this operation cannot be realized
		if free_slot.count(0) < self.myAgent.duration:
			self.myAgent.addBehaviour(ResponseAddEventFALSE())
		# Get two index (free slots) and shift the events between them	
		else:
			while(True):
				start = free_slot.index(0)
				free_slot[start] = 1
				end = free_slot.index(0)
				# Assumes that no one event have duration more longer that 2 hours
				if start + 1 != end: 
					if movable[start + 1] > 0 and priority[start + 1] <= self.myAgent.priority:
						free_slot[start] = 1
						free_slot[end - 1] = 0
						self.shift(start, end)
						status = self.tryPutEvent()
						if status == True:
							self.myAgent.addBehaviour(ResponseAddEventTRUE())		
						else:
							self.myAgent.addBehaviour(ResponseAddEventFALSE())		 	
						break
				else:
					self.myAgent.addBehaviour(ResponseAddEventFALSE())
					break

	def notAvailable(self, indice, duration):
		global free_slot
		free_slot[indice] = 1
		while duration > 1:
			indice += 1
			free_slot[indice] = 1
			duration -= 1
		print free_slot	

	def shift(self, start, end):
		with open(self.myAgent.jid + ".txt", 'r+') as f:
			lines = f.readlines()
			f.seek(0)
			for line in lines:
				event = str(line.split(' ')[1]) 
				hour = int(line.split(' ')[3])
				day = str(line.split(' ')[5])
				month = str(line.split(' ')[7])
				year = str(line.split(' ')[9])
				duration = str(line.split(' ')[11])
				priority = str(line.split(' ')[13])
				if "MOVABLE" in line:
					movable = "MOVABLE" 
				else:
					movable = ""  	
				if hour > (start + 8) and hour < (end + 8):
					new_hour = hour - 1
					f.write("Event: "+event+ " Hour: "+str(new_hour)+" Day: "+day+" Month: "+month+" Year: "+year+\
							" Duration: "+duration+" Priority: "+priority+" "+movable+"\n")
				else:
					f.write(line)
			f.truncate()
			f.close()

	def tryPutEvent(self):
		global free_slot
		start = free_slot.index(0)
		free_slot[start] = 1
		end = free_slot.index(0)
		print "Start: "+str(start)+" End: "+str(end)
		if (end - start + 1) >= int(self.myAgent.duration):
				# start + 8, because start is the position of vector free_slot and 8 is from 8am
				self.myAgent.hour = start + 8
				if self.myAgent.event == " ":
						self.myAgent.event = "Random Event"
				with open(self.myAgent.jid + ".txt", 'a') as f:	
					f.write("Event: "+self.myAgent.event+ " Hour: "+str(self.myAgent.hour)+" Day: "+str(self.myAgent.day)\
						+" Month: "+str(self.myAgent.month)+" Year: "+str(self.myAgent.year)+" Duration: "+\
						str(self.myAgent.duration)+" Priority: "+str(self.myAgent.priority)+" MOVABLE"+"\n")	
					f.truncate()
					f.close()
				return True
		else:
			#free_slot[start] = 0		
			return False