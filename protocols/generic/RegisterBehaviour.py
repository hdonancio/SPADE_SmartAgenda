import spade
	
class RegisterBehaviour(spade.Behaviour.OneShotBehaviour):
	def onStart(self):			
		aad = spade.AMS.AmsAgentDescription()
		if(self.myAgent.role == "R1"):						
			aad.ownership = "ManagerOwner"
		elif(self.myAgent.role == "R2"): 			
			aad.ownership = "AgendaOwner"
		elif(self.myAgent.role == "R3"): 	
			aad.ownership = "AssistantOwner"	
		print "Agent Registered", str(self.myAgent.getAID())
	
	
