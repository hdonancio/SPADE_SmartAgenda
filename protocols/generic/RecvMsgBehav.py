import spade 
import datetime
from ProtocolSubscribe import StartProtocolSubscribe
from protocols.manager.ProtocolManager import StartProtocolManager
from protocols.agenda.ProtocolAgenda import StartProtocolAgenda
from protocols.group.ProtocolGroup import StartProtocolGroupReq
from protocols.group.ProtocolGroup import StartProtocolGroup

# This behaviour has been triggered by a message of Coordinator
class ReceiveMessageBehaviourC(spade.Behaviour.EventBehaviour):       
	def _process(self):
		msg = self._receive(True)
		self.myAgent.msg = msg
		self.myAgent.performative = msg.getPerformative()
		self.myAgent.protocol = msg.getProtocol()
		self.myAgent.content = msg.getContentObject()
		self.myAgent.addBehaviour(MessageProcessingBehaviour())		

# This behaviour has been triggered by a message of Manager
class ReceiveMessageBehaviourM(spade.Behaviour.EventBehaviour):       
	def _process(self):
		msg = self._receive(True)
		self.myAgent.msg = msg
		self.myAgent.performative = msg.getPerformative()
		self.myAgent.protocol = msg.getProtocol()
		self.myAgent.content = msg.getContentObject()
		self.myAgent.addBehaviour(MessageProcessingBehaviour())				

# This behaviour has been triggered by a message of Agenda (usefull to groups)
class ReceiveMessageBehaviourA(spade.Behaviour.EventBehaviour):       
	def _process(self):
		msg = self._receive(True)
		self.myAgent.msg = msg
		self.myAgent.performative = msg.getPerformative()
		self.myAgent.protocol = msg.getProtocol()
		self.myAgent.ontology = msg.getOntology()
		self.myAgent.content = msg.getContentObject()
		self.myAgent.addBehaviour(MessageProcessingBehaviour())						

# This behaviour has been triggered by a message of Myself (usefull to Automatic Behaviours)
class ReceiveMessageBehaviourMy(spade.Behaviour.EventBehaviour):       
	def _process(self):
		msg = self._receive(True)
		self.myAgent.msg = msg
		self.myAgent.performative = msg.getPerformative()
		self.myAgent.protocol = msg.getProtocol()
		self.myAgent.ontology = msg.getOntology()
		self.myAgent.content = msg.getContentObject()
		self.myAgent.addBehaviour(MessageProcessingBehaviour())								

# This behaviour has been triggered by a message of Agenda with Performative AGREE
class ReceiveMessageBehaviourAA(spade.Behaviour.EventBehaviour):       
	def _process(self):
		msg = self._receive(True)
		self.myAgent.msg = msg
		self.myAgent.performative = msg.getPerformative()
		self.myAgent.protocol = msg.getProtocol()
		self.myAgent.content = msg.getContentObject()
		self.myAgent.addBehaviour(MessageProcessingBehaviour())		

# This behaviour has been triggered by a message of Agenda with Performative FAILURE
class ReceiveMessageBehaviourAF(spade.Behaviour.EventBehaviour):       
	def _process(self):
		msg = self._receive(True)
		self.myAgent.msg = msg
		self.myAgent.performative = msg.getPerformative()
		self.myAgent.protocol = msg.getProtocol()
		self.myAgent.content = msg.getContentObject()
		self.myAgent.addBehaviour(MessageProcessingBehaviour())		

# This behaviour has been triggered by a message of Manager to Coordinator
class ReceiveMessageBehaviourMC(spade.Behaviour.EventBehaviour):       
	global num_msgs
	num_msgs = 0

	def _process(self):
		global num_msgs
		msg = self._receive(True)
		if msg:
			num_msgs += 1
			self.myAgent.msg = msg
			self.myAgent.performative = msg.getPerformative()
			self.myAgent.protocol = msg.getProtocol()
			self.myAgent.content = msg.getContentObject()
			now = str(datetime.datetime.now())
			print now + "||message arrived"

			if(self.myAgent.content.__getattr__("rdf:response") != None):
				self.myAgent.status = str(self.myAgent.content.__getattr__("rdf:response"))

	def getNumOfMsgs(self):
		global num_msgs
		return num_msgs		

class MessageProcessingBehaviour(spade.Behaviour.OneShotBehaviour):       		    
	def _process(self):	
		protocol = self.myAgent.protocol
		if(protocol == "Subscribe"):
			self.myAgent.addBehaviour(StartProtocolSubscribe())
		elif(protocol == "Agenda"): 	
			self.myAgent.addBehaviour(StartProtocolAgenda())
		elif(protocol == "Assistant"): 	
			pass
		elif(protocol == "Manager"): 	
			self.myAgent.addBehaviour(StartProtocolManager())		
		elif(protocol == "GroupReq"): 	
			self.myAgent.addBehaviour(StartProtocolGroupReq())		
		elif(protocol == "Group"): 	
			self.myAgent.addBehaviour(StartProtocolGroup())			
		
