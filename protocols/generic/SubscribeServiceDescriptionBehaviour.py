import spade

class SubscribeServiceDescriptionBehaviour(spade.Behaviour.OneShotBehaviour):
	def onStart(self):
		sd = spade.DF.ServiceDescription()
		#My service
		#sd.setName(self.myAgent.jid) 
		sd.setName(self.myAgent.register) 
		if(self.myAgent.role == "R1"):			
			#My type
			sd.setType("ManagerAgent")
		elif(self.myAgent.role == "R2"): 			
			sd.setType("AgendaAgent")	
		elif(self.myAgent.role == "R3"): 	
			sd.setType("AssistantAgent")		
		dad = spade.DF.DfAgentDescription()
		dad.addService(sd)
		dad.setAID(self.myAgent.getAID())
		res = self.myAgent.registerService(dad)
		print "Service Registered:",str(res)

class UnsubscribeServiceDescriptionBehavior(spade.Behaviour.OneShotBehaviour):
	def onStart(self):
		print "UnsubscribeServiceDescriptionBehavior"
		s = self.myAgent.searchService(spade.DF.Service(name=self.myAgent.deregister))
		if s: 
			self.myAgent.deregisterService(s[0])
			print "Unsubscribe completed"

