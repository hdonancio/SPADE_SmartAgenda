import spade
from AssistantAgent import AssistantAgent
from AgendaAgent import AgendaAgent

class StartProtocolSubscribe(spade.Behaviour.OneShotBehaviour):
	def _process(self):			
		performative = self.myAgent.performative
		content = self.myAgent.content
		if(performative == "request"):
			# Preferred hours for automatic filling behavior
			self.myAgent.hour_1 = int(self.myAgent.content.__getattr__("rdf:hour_1"))
			self.myAgent.hour_2 = int(self.myAgent.content.__getattr__("rdf:hour_2"))
			self.myAgent.hour_3 = int(self.myAgent.content.__getattr__("rdf:hour_3"))
			self.myAgent.hour_4 = int(self.myAgent.content.__getattr__("rdf:hour_4"))
			self.myAgent.hour_5 = int(self.myAgent.content.__getattr__("rdf:hour_5"))								

			# Creates an Assistant Agent
			self.myAgent.addBehaviour(CreateAccount())

			# Creates an Agenda Agent
			self.myAgent.addBehaviour(CreateAgendaAgent())
			
		elif(performative == "inform"):
			pass	
		elif(performative == "subscribe"):
			pass		

class CreateAccount(spade.Behaviour.OneShotBehaviour):
	def _process(self):			
		jid = self.myAgent.user_name + "_assistant@127.0.0.1"
		jid = jid.lower()
		password = self.myAgent.password
		a = AssistantAgent(jid, password)
		a.setNameUser(jid)	
		a.setManagerJID(self.myAgent.jid)	
		a.start()

class CreateAgendaAgent(spade.Behaviour.OneShotBehaviour):
	def _process(self):			
		jid = self.myAgent.user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		password = self.myAgent.password
		a = AgendaAgent(jid, password)
		a.setNameUser(jid)	
		a.setManagerJID(self.myAgent.jid)	
		a.setPreferredHour(self.myAgent.hour_1, self.myAgent.hour_2, self.myAgent.hour_3,\
		 self.myAgent.hour_4, self.myAgent.hour_5)
		a.start()

		self.myAgent.status = True
		self.myAgent.addBehaviour(SendNotificationToCoordinator())

class SendNotificationToCoordinator(spade.Behaviour.OneShotBehaviour):								
	def _process(self):
		print "SendNotificationToCoordinator"

		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setOntology("tocoordinator")
		self.msg.setProtocol("Coordinator")          
		
		receiver = spade.AID.aid(name= "coordinator@127.0.0.1", 
                             addresses=["xmpp://coordinator@127.0.0.1"])
		self.msg.addReceiver(receiver)               

		self.answer = spade.content.ContentObject()
		if self.myAgent.status == True:
			self.answer["response"] = "success"
		else:
			self.answer["response"] = "failed"	
		self.msg.setContentObject(self.answer)       
	
		self.myAgent.send(self.msg)		
