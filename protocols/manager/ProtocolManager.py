import spade
import time
import datetime


# Here, all the operations are filtered to be treated by a specific behavior
#  according to the performative of the received message
class StartProtocolManager(spade.Behaviour.OneShotBehaviour):
	def _process(self):			
		performative = self.myAgent.performative
		if(performative == "request"):				
			self.myAgent.addBehaviour(ExecuteRequest())
		elif(performative == "confirm"):
			pass	
		elif(performative == "disconfirm"):
			pass		
		elif(performative == "agree"):
			content = self.myAgent.content
			if(content.__getattr__("rdf:agenda") != None):
				self.myAgent.addBehaviour(RetrieveAgendaToCoordinator())			
			else:	
				self.myAgent.status = True
				self.myAgent.addBehaviour(SendNotificationToCoordinator())
		elif(performative == "failure"):
			self.myAgent.status = False	
			self.myAgent.addBehaviour(SendNotificationToCoordinator())

class ExecuteRequest(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		content = self.myAgent.content
		if(content.__getattr__("rdf:name") != None):
			self.myAgent.addBehaviour(ExecuteRequestCreateAccount())
		elif(content.__getattr__("rdf:event") != None):
			if(content.__getattr__("rdf:hour") == ""):
				self.myAgent.addBehaviour(ExecuteRequestAskAddEvent())
			else:	
				self.myAgent.addBehaviour(ExecuteRequestAddEvent())
		elif (content.__getattr__("rdf:user_name") != None):	
			if (content.__getattr__("rdf:remove") != None):	
				self.myAgent.addBehaviour(ExecuteRequestRemoveEvent())
			else:
				self.myAgent.addBehaviour(ExecuteRequestViewAgenda())
		elif (content.__getattr__("rdf:new_event") != None):
			self.myAgent.addBehaviour(ExecuteRequestModifyEvent())	
		elif (content.__getattr__("rdf:create") != None):		
			self.myAgent.addBehaviour(ExecuteRequestCreateGroup())
		elif (content.__getattr__("rdf:subscribe") != None):		
			self.myAgent.addBehaviour(ExecuteRequestSubscribeGroup())	
		elif (content.__getattr__("rdf:leave") != None):		
			self.myAgent.addBehaviour(ExecuteRequestLeaveGroup())		
		else:
			pass

class ExecuteRequestCreateAccount(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		jid = self.myAgent.jid
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg = spade.ACLMessage.ACLMessage()     # Instantiate the message
		self.msg.setPerformative("request")          # Set the "request" FIPA performative
		self.msg.setProtocol("Subscribe")            # Set the protocol of the message
		self.msg.addReceiver(receiver)               # Add the message receiver
		#Its necessary unmount the received message because this have some content relationed
		# to fipa protocol
		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:name"))
		self.myAgent.transport1 = str(self.myAgent.content.__getattr__("rdf:transport1"))
		self.myAgent.transport2 = str(self.myAgent.content.__getattr__("rdf:transport2"))
		self.myAgent.transport3 = str(self.myAgent.content.__getattr__("rdf:transport3"))
		self.myAgent.hotel = str(self.myAgent.content.__getattr__("rdf:hotel"))
		self.myAgent.hour_1 = str(self.myAgent.content.__getattr__("rdf:hour_1"))
		self.myAgent.hour_2 = str(self.myAgent.content.__getattr__("rdf:hour_2"))
		self.myAgent.hour_3 = str(self.myAgent.content.__getattr__("rdf:hour_3"))
		self.myAgent.hour_4 = str(self.myAgent.content.__getattr__("rdf:hour_4"))
		self.myAgent.hour_5 = str(self.myAgent.content.__getattr__("rdf:hour_3"))

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["transport1"] = self.myAgent.transport1
		self.user_data["transport2"] = self.myAgent.transport2
		self.user_data["transport3"] = self.myAgent.transport3
		self.user_data["hotel"] = self.myAgent.hotel	
		self.user_data["hour_1"] = self.myAgent.hour_1
		self.user_data["hour_2"] = self.myAgent.hour_2
		self.user_data["hour_3"] = self.myAgent.hour_3
		self.user_data["hour_4"] = self.myAgent.hour_4
		self.user_data["hour_5"] = self.myAgent.hour_5
		self.msg.setContentObject(self.user_data)  	  # Set the message ContentObject

		self.myAgent.send(self.msg)		
		
class ExecuteRequestAddEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("inform")           
		self.msg.setProtocol("Agenda")               

		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:user"))
		self.myAgent.day = str(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = str(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = str(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.hour = str(self.myAgent.content.__getattr__("rdf:hour"))
		self.myAgent.duration = str(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		self.myAgent.movable = str(self.myAgent.content.__getattr__("rdf:movable"))
		self.myAgent.user_event = str(self.myAgent.content.__getattr__("rdf:event"))
		jid = self.myAgent.user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["day"] = self.myAgent.day
		self.user_data["month"] = self.myAgent.month
		self.user_data["year"] = self.myAgent.year
		self.user_data["hour"] = self.myAgent.hour	
		self.user_data["duration"] = self.myAgent.duration
		self.user_data["priority"] = self.myAgent.priority	
		self.user_data["movable"] = self.myAgent.movable	
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ExecuteRequestAskAddEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("propose")           
		self.msg.setProtocol("Agenda")               

		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:user"))
		self.myAgent.day = str(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = str(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = str(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.duration = str(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		self.myAgent.movable = str(self.myAgent.content.__getattr__("rdf:movable"))
		self.myAgent.user_event = str(self.myAgent.content.__getattr__("rdf:event"))
		jid = self.myAgent.user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["day"] = self.myAgent.day
		self.user_data["month"] = self.myAgent.month
		self.user_data["year"] = self.myAgent.year
		self.user_data["duration"] = self.myAgent.duration
		self.user_data["priority"] = self.myAgent.priority	
		self.user_data["movable"] = self.myAgent.movable	
		self.user_data["event"] = self.myAgent.user_event	
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class ExecuteRequestModifyEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("inform_if")        
		self.msg.setProtocol("Agenda")               

		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:usr_name"))
		self.myAgent.modify = str(self.myAgent.content.__getattr__("rdf:modify"))
		self.myAgent.day = str(self.myAgent.content.__getattr__("rdf:new_day"))
		self.myAgent.month = str(self.myAgent.content.__getattr__("rdf:new_month"))
		self.myAgent.year = str(self.myAgent.content.__getattr__("rdf:new_year"))
		self.myAgent.hour = str(self.myAgent.content.__getattr__("rdf:new_hour"))
		self.myAgent.user_event = str(self.myAgent.content.__getattr__("rdf:new_event"))
		self.myAgent.duration = str(self.myAgent.content.__getattr__("rdf:new_duration"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:new_priority"))
		self.myAgent.movable = str(self.myAgent.content.__getattr__("rdf:movable"))

		jid = self.myAgent.user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["modify"] = self.myAgent.modify
		self.user_data["day"] = self.myAgent.day
		self.user_data["month"] = self.myAgent.month
		self.user_data["year"] = self.myAgent.year
		self.user_data["hour"] = self.myAgent.hour	
		self.user_data["duration"] = self.myAgent.duration	
		self.user_data["priority"] = self.myAgent.priority	
		self.user_data["movable"] = self.myAgent.movable	
		self.user_data["event"] = self.myAgent.user_event
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		
	
class ExecuteRequestRemoveEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("inform_ref")       
		self.msg.setProtocol("Agenda")               

		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:user_name"))
		self.myAgent.remove = str(self.myAgent.content.__getattr__("rdf:remove"))
		jid = self.myAgent.user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.user_data["event"] = self.myAgent.remove
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)						

class ExecuteRequestViewAgenda(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("request")          
		self.msg.setProtocol("Agenda")               
		
		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:user_name"))
		jid = self.myAgent.user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = self.myAgent.user_name
		self.msg.setContentObject(self.user_data)    

		self.myAgent.send(self.msg)		

class SendNotificationToCoordinator(spade.Behaviour.OneShotBehaviour):								
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setOntology("tocoordinator")
		self.msg.setProtocol("Coordinator")          
		
		receiver = spade.AID.aid(name= "coordinator@127.0.0.1", 
                             addresses=["xmpp://coordinator@127.0.0.1"])
		self.msg.addReceiver(receiver)               

		self.answer = spade.content.ContentObject()
		if self.myAgent.status == True:
			self.answer["response"] = "success"
		else:
			self.answer["response"] = "failed"	
		self.msg.setContentObject(self.answer)       
	
		self.myAgent.send(self.msg)		

class RetrieveAgendaToCoordinator(spade.Behaviour.OneShotBehaviour):								
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("inform") 
		self.msg.setOntology("tocoordinator")           
		self.msg.setProtocol("Coordinator")
		
		receiver = spade.AID.aid(name= "coordinator@127.0.0.1", 
                             addresses=["xmpp://coordinator@127.0.0.1"])
		self.msg.addReceiver(receiver)               

		self.myAgent.user_name = str(self.myAgent.content.__getattr__("rdf:name"))
		self.myAgent.agenda = str(self.myAgent.content.__getattr__("rdf:agenda"))
	
		self.answer = spade.content.ContentObject()
		self.answer["name"] = self.myAgent.user_name
		self.answer["agenda"] = self.myAgent.agenda
		self.msg.setContentObject(self.answer)       

		self.myAgent.send(self.msg)		