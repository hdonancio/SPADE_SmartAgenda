from random import choice

# [P] Implementar um check de id unica
class GenerateEventID(object):
	def generate(self, group):
		chars = "123456789"

		id_ = ''
		for c in range(10):
			id_ = choice(chars) + id_
		id_ = group + id_	
		return id_	

class CheckColisions(object):
	def check(self, user_name, event_hour, event_day, event_month, event_year, event_duration):
		#[P] Change open() to relative diretory
		with open("/home/henrique/Documents/Research/Smart_Agenda_Project/Agent/SPADE_SmartAgenda/"+ user_name + ".txt", 'r+') as f:
			lines = f.readlines()
			if not lines == '':
				return False
			for line in lines:
				hour = int(line.split(' ')[3])
				day = int(line.split(' ')[5])
				month = int(line.split(' ')[7])
				year = int(line.split(' ')[9])
				duration = int(line.split(' ')[11])
				if year != event_year:
					pass
				elif month != event_month:
					pass
				elif day != event_day:
					pass
				elif event_hour < hour and event_hour + event_duration < hour:
					pass
				elif event_hour > hour and event_hour > hour + duration:
					pass
				else:
					return True	
			return False

class FindBetween(object):
	# This method extracts the OWNERSHIP of one Service Description in DF. The OWNERSHIP is the user 
	#  that propose a group event.		
	def find_between(self, s, first, last ):
		try:
		    start = s.index( first ) + len( first )
		    end = s.index( last, start )
		    #print "FIND BETWEEN: " + s[start:end]
		    return s[start:end]
		except ValueError:
		    return ""

# Store Events ID's created by user. But its not persistent. [Q] Should be
class CreatedEvents(object):
        events = []