import spade
import time
from CheckCollisions import CheckCollisions
from Util import GenerateEventID
from Util import FindBetween
from Util import CreatedEvents

# Relative import don't work for some reason. I duplicate it in the group folder
from SubscribeServiceDescriptionBehaviour import SubscribeServiceDescriptionBehaviour
from SubscribeServiceDescriptionBehaviour import UnsubscribeServiceDescriptionBehavior

class StartProtocolGroupReq(spade.Behaviour.OneShotBehaviour):
	def _process(self):	
		performative = self.myAgent.performative
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setProtocol("Group")               

		user_name = str(self.myAgent.content.__getattr__("rdf:user_group"))
		group_name = str(self.myAgent.content.__getattr__("rdf:group"))

		jid = user_name + "_agenda@127.0.0.1"
		jid = jid.lower()
		receiver = spade.AID.aid(name= jid, 
                             addresses=["xmpp://" + jid])
		self.msg.addReceiver(receiver)               

		self.user_data = spade.content.ContentObject()
		self.user_data["name"] = user_name
		self.user_data["group"] = group_name
		if(performative == "request"):	
			if self.myAgent.content.__getattr__("rdf:create") != None:
				self.msg.setPerformative("subscribe")           
				self.user_data["create"] = "true"
			elif self.myAgent.content.__getattr__("rdf:subscribe") != None:	
				self.msg.setPerformative("subscribe")           
				self.user_data["subscribe"] = "true"
			elif self.myAgent.content.__getattr__("rdf:leave") != None:			
				self.msg.setPerformative("cancel")           
				self.user_data["leave"] = "true"
			elif self.myAgent.content.__getattr__("rdf:group_event") != None:				
				self.msg.setPerformative("inform")
				group_event = str(self.myAgent.content.__getattr__("rdf:group_event"))    
				event = str(self.myAgent.content.__getattr__("rdf:event"))    
				day = str(self.myAgent.content.__getattr__("rdf:day"))
				month = str(self.myAgent.content.__getattr__("rdf:month"))
				year = str(self.myAgent.content.__getattr__("rdf:year"))
				hour = str(self.myAgent.content.__getattr__("rdf:hour"))
				duration = str(self.myAgent.content.__getattr__("rdf:duration"))
				priority = str(self.myAgent.content.__getattr__("rdf:priority"))    
				self.user_data["group_event"] = group_event
				self.user_data["event"] = event
				self.user_data["day"] = day
				self.user_data["month"] = month
				self.user_data["year"] = year
				self.user_data["hour"] = hour	
				self.user_data["duration"] = duration
				self.user_data["priority"] = priority
				if self.myAgent.content.__getattr__("rdf:optional") != None:				
					self.user_data["optional"] = "true"
			elif self.myAgent.content.__getattr__("rdf:remove") != None: 		
				self.msg.setPerformative("inform-ref")
				event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))    
				remove = str(self.myAgent.content.__getattr__("rdf:remove"))    
				self.user_data["event_id"] = event_id
				self.user_data["remove"] = remove
			elif self.myAgent.content.__getattr__("rdf:modify") != None: 	
				self.msg.setPerformative("inform-if")
				event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))    
				modify = str(self.myAgent.content.__getattr__("rdf:modify"))    
				day = str(self.myAgent.content.__getattr__("rdf:new_day"))
				month = str(self.myAgent.content.__getattr__("rdf:new_month"))
				year = str(self.myAgent.content.__getattr__("rdf:new_year"))
				hour = str(self.myAgent.content.__getattr__("rdf:new_hour"))
				event = str(self.myAgent.content.__getattr__("rdf:new_event"))
				duration = str(self.myAgent.content.__getattr__("rdf:new_duration"))
				priority = str(self.myAgent.content.__getattr__("rdf:new_priority"))
				optional = str(self.myAgent.content.__getattr__("rdf:optional"))
				self.user_data["event_id"] = event_id
				self.user_data["modify"] = modify
				self.user_data["event"] = event
				self.user_data["day"] = day
				self.user_data["month"] = month
				self.user_data["year"] = year
				self.user_data["hour"] = hour	
				self.user_data["duration"] = duration
				self.user_data["priority"] = priority	
				self.user_data["optional"] = optional	
			self.msg.setContentObject(self.user_data)    

			self.myAgent.send(self.msg)			

class StartProtocolGroup(spade.Behaviour.OneShotBehaviour):
	def _process(self):	
		performative = self.myAgent.performative
		try:
			ontology = self.myAgent.ontology	
		except:
			ontology = None	
		if(performative == "subscribe"):	
			if self.myAgent.content.__getattr__("rdf:create") != None:
				self.myAgent.addBehaviour(CreateGroup())
			elif self.myAgent.content.__getattr__("rdf:subscribe") != None:	
				self.myAgent.addBehaviour(ExecuteSubscribeGroup())
		elif(performative == "cancel"):			
			self.myAgent.addBehaviour(ExecuteLeaveGroup())
		elif(performative == "inform"):
			self.myAgent.addBehaviour(AskGroupAvailability())
		elif(performative == "propose"):
			self.myAgent.addBehaviour(AnswerSlotAvailability())	
		elif(performative == "inform-ref"):		
			self.myAgent.addBehaviour(RemoveGroupEvent())	
		elif(performative == "inform-if"):		
			self.myAgent.addBehaviour(ModifyGroupEventMessage())		
		elif(performative == "request"):		
			self.myAgent.addBehaviour(ExecuteRemoveOptionalGroupEvent())
		elif(performative == "disconfirm"):		
			self.myAgent.addBehaviour(ExecuteRemoveGroupEvent())			
		elif(performative == "propagate"):		
			self.myAgent.addBehaviour(ExecuteModifyGroupEvent())
		elif(performative == "accept-proposal"):	
			self.myAgent.addBehaviour(RespondeAddGroupEvent())
		elif(performative == "confirm"):	
			self.myAgent.addBehaviour(ExecuteAddGroupEvent())	
		else:
			pass	
		
class CreateGroup(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		group_name = str(self.myAgent.content.__getattr__("rdf:group"))

		sd = spade.DF.ServiceDescription()
		sd.setName(group_name) 
		if(self.myAgent.role == "R1"):			
			sd.setType("ManagerAgent")
		elif(self.myAgent.role == "R2"): 			
			sd.setType("AgendaAgent")	
		elif(self.myAgent.role == "R3"): 	
			sd.setType("AssistantAgent")		
		dad = spade.DF.DfAgentDescription()
		dad.addService(sd)
		dad.setAID(self.myAgent.getAID())
		res = self.myAgent.registerService(dad)
		print "Create Group:",str(res)
		#[P] Enviar para coordinator  - enviei apenas a confirmacao
		self.myAgent.addBehaviour(SendNotificationToManager())

# These methods have been adapted to meet project requirements. They were placed within AgendaProtocol
#  because SubscribeServiceDescriptionBehaviour don't receive parameters
class ExecuteSubscribeGroup(spade.Behaviour.OneShotBehaviour):
	def _process(self):		
		group_name = str(self.myAgent.content.__getattr__("rdf:group"))
		print "SEARCHING GROUP..."
		s = self.myAgent.searchService(spade.DF.Service(name=group_name))
		self.myAgent.DEBUG("Found service " + str(s[0]),'ok')

		sd = spade.DF.ServiceDescription()
		sd.setName(group_name) 
		if(self.myAgent.role == "R1"):			
			sd.setType("ManagerAgent")
		elif(self.myAgent.role == "R2"): 			
			sd.setType("AgendaAgent")	
		elif(self.myAgent.role == "R3"): 	
			sd.setType("AssistantAgent")		
		dad = spade.DF.DfAgentDescription()
		dad.addService(sd)
		dad.setAID(self.myAgent.getAID())
		res = self.myAgent.registerService(dad)
		print "Subscribe completed:",str(res)

		self.myAgent.addBehaviour(SendNotificationToManager())

class ExecuteLeaveGroup(spade.Behaviour.OneShotBehaviour):
	def _process(self):		
		group_name = str(self.myAgent.content.__getattr__("rdf:group"))

		s = self.myAgent.searchService(spade.DF.Service(name=group_name))
		if s: 
			self.myAgent.deregisterService(s[0])
			print "Leave Group Completed"

		self.myAgent.addBehaviour(SendNotificationToManager())	

class AskGroupAvailability(spade.Behaviour.OneShotBehaviour):		
	def _process(self):
		global address_list
		address_list = []

		self.myAgent.day = int(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = int(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = int(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.hour = int(self.myAgent.content.__getattr__("rdf:hour"))
		self.myAgent.duration = int(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.event = str(self.myAgent.content.__getattr__("rdf:event"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		obj = CheckCollisions()	
		self.myAgent.collision = obj.check(self.myAgent.jid, self.myAgent.hour, self.myAgent.day,\
		 self.myAgent.month, self.myAgent.year, self.myAgent.duration)

		self.myAgent.group_name = str(self.myAgent.content.__getattr__("rdf:group_event"))
		s = self.myAgent.searchService(spade.DF.Service(name=self.myAgent.group_name))
		if self.myAgent.collision == False and len(s) > 1:
			# Number of answer pendents before confirm event. If the event is OPTIONAL, only one answer
			#  is satisfactory, otherwise, if the event is MANDATORY, all users in group needs confirm.
			if self.myAgent.content.__getattr__("rdf:optional") != None:				
				self.myAgent.optional = "true"
				template1 = spade.Behaviour.ACLTemplate()
				template1.setOntology("optional")
				t1 = spade.Behaviour.MessageTemplate(template1)
				self.myAgent.addBehaviour(ReceiveOptionalGroupAnswerMessages(), t1)
			else:
				self.myAgent.optional = "false"
				template2 = spade.Behaviour.ACLTemplate()
				template2.setOntology("mandatory")
				t2 = spade.Behaviour.MessageTemplate(template2)
				self.myAgent.addBehaviour(ReceiveMandatoryGroupAnswerMessages(), t2)

			# Total number of users in group group_name
			self.myAgent.num_usr_group = len(s) - 1
			self.myAgent.pendent_answer	= self.myAgent.num_usr_group

			# Each event have an unique ID
			obj = GenerateEventID()
			event_id = obj.generate(self.myAgent.group_name)
			self.myAgent.event_id = event_id

			self.data = spade.content.ContentObject()
			self.data["host"] = str(self.myAgent.jid)
			self.data["day"] = str(self.myAgent.day)
			self.data["month"] = str(self.myAgent.month)
			self.data["year"] = str(self.myAgent.year)
			self.data["hour"] = str(self.myAgent.hour)
			self.data["duration"] = str(self.myAgent.duration)
			self.data["event"] = str(self.myAgent.event)
			self.data["priority"] = str(self.myAgent.priority)
			self.data["optional"] = str(self.myAgent.optional)
			self.data["group_name"] = str(self.myAgent.group_name)

			# Just for test. Its not persistent (this is bad)
			obj = CreatedEvents()
			obj.events.append(event_id)

			self.data["event_id"] = self.myAgent.event_id 

			i = 0
			while i < len(s):
				start = "<ownership>"
				end = "</ownership>"
				stringSubject = str(s[i])
				obj = FindBetween()
				address = obj.find_between(stringSubject, start, end)
				
				# Invites only others users of group (exclude myself)
				if address != self.myAgent.jid and address not in address_list:
					# This list store all address because later on the host needs to confirm
					#  the event to others participants
					address_list.append(address)
					print address + "/" + self.myAgent.jid

					receiver = spade.AID.aid(name=address, 
	                                 addresses=["xmpp://" + address])	

					self.msg = spade.ACLMessage.ACLMessage()   # Instantiate the message
					self.msg.setPerformative("propose")        # Set the "request" FIPA performative
					self.msg.setProtocol("Group")              # Set the protocol of the message
					self.msg.setOntology("group_event")		   # Set the ontology of the message
					self.msg.setContentObject(self.data)       # Set the message content	
					self.msg.addReceiver(receiver)             # Add the message receiver
					self.myAgent.send(self.msg)		

				i += 1

				# Sleep is necessary to update pendent_answer variable
				time.sleep(3)
		
class AnswerSlotAvailability(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.myAgent.day = int(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = int(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = int(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.hour = int(self.myAgent.content.__getattr__("rdf:hour"))
		self.myAgent.duration = int(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.event = str(self.myAgent.content.__getattr__("rdf:event"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		self.myAgent.optional = str(self.myAgent.content.__getattr__("rdf:optional"))
		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))
		self.myAgent.group_name = str(self.myAgent.content.__getattr__("rdf:group_name"))
		host = str(self.myAgent.content.__getattr__("rdf:host"))

		self.msg = spade.ACLMessage.ACLMessage()  

		obj = CheckCollisions()	
		self.myAgent.collision = obj.check(self.myAgent.jid, self.myAgent.hour, self.myAgent.day,\
		 self.myAgent.month, self.myAgent.year, self.myAgent.duration)

		if self.myAgent.collision == False:
			self.msg.setPerformative("accept-proposal")   
			if self.myAgent.optional == "true":
				self.myAgent.addBehaviour(ExecuteAddGroupEvent())
		if self.myAgent.collision == True: 		
			self.msg.setPerformative("reject-proposal")        

		self.data = spade.content.ContentObject()
		self.data["event"] = self.myAgent.event
		self.data["guest"] = self.myAgent.jid
		self.data["optional"] = self.myAgent.optional

		receiver = spade.AID.aid(name=host, 
                         addresses=["xmpp://" + host])	

		self.msg.setProtocol("Group")              
		if self.myAgent.optional == "true":
			self.msg.setOntology("optional")		   
		elif self.myAgent.optional == "false":	
			self.msg.setOntology("mandatory")		   
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(self.data)       

		self.myAgent.send(self.msg)	

class ExecuteAddGroupEvent(spade.Behaviour.OneShotBehaviour):		
	def _process(self):
		f = open(self.myAgent.jid + ".txt","a+")
		
		event = self.myAgent.event
		day = str(self.myAgent.day)
		month = str(self.myAgent.month)
		year = str(self.myAgent.year)
		hour = str(self.myAgent.hour)
		duration = str(self.myAgent.duration)
		priority = self.myAgent.priority
		event_id = self.myAgent.event_id
		group_name = self.myAgent.group_name
		optional = self.myAgent.optional
		# If the event is OPTIONAL, add more one dollar sign
		if optional == "true":
			optional = "$"
		else:
			optional = ""	
			
		f.write("Event: "+event+ " Hour: "+hour+" Day: "+day+" Month: "+month+" Year: "\
			+year+" Duration: "+duration+" Priority: "+priority+" Group: "+group_name+" $"\
			+event_id+"%"+optional+"\n")
		f.close()

		# Register event as service in DF
		self.myAgent.register = event_id
		self.myAgent.addBehaviour(SubscribeServiceDescriptionBehaviour())
		
class ReceiveOptionalGroupAnswerMessages(spade.Behaviour.EventBehaviour):
	def _process(self):
		msg = None
		
		# Blocking receive indefinitely
		msg = self._receive(True)
		
		# Check wether the message arrived
		if msg:
			self.myAgent.pendent_answer	-= 1
			self.myAgent.msg = msg
			self.myAgent.performative = msg.getPerformative()
			self.myAgent.protocol = msg.getProtocol()
			self.myAgent.content = msg.getContentObject()
			if self.myAgent.performative == 'accept-proposal':
				self.myAgent.addBehaviour(ExecuteAddGroupEvent())
				self.myAgent.addBehaviour(SendNotificationToManager())
		else:
			print "Waiting..."

class ReceiveMandatoryGroupAnswerMessages(spade.Behaviour.EventBehaviour):
	def _process(self):
		global address_list

		msg = None
		
		# Blocking receive indefinitely
		msg = self._receive(True)
		
		# Check wether the message arrived
		if msg:
			self.myAgent.pendent_answer	-= 1
			print "Pendent Answers: " + str(self.myAgent.pendent_answer)
			self.myAgent.msg = msg
			self.myAgent.performative = msg.getPerformative()
			self.myAgent.protocol = msg.getProtocol()
			self.myAgent.content = msg.getContentObject()

			if self.myAgent.performative == "reject-proposal":
				#[Q] Consigo remover um Behaviour dentro dentro do mesmo
				self.myAgent.removeBehaviour(ReceiveMandatoryGroupAnswerMessages())

			elif self.myAgent.pendent_answer == 0:

				self.myAgent.addBehaviour(ExecuteAddGroupEvent())	
				self.myAgent.addBehaviour(SendNotificationToManager())
				
				# This is redundant, but guarantees more confiability that last content is
				#  the respective event the needs be add
				self.data = spade.content.ContentObject()
				# self.data["event"] = self.myAgent.event
				# self.data["day"] = self.myAgent.day
				# self.data["month"] = self.myAgent.month
				# self.data["year"] = self.myAgent.year
				# self.data["hour"] = self.myAgent.hour
				# self.data["duration"] = self.myAgent.duration
				# self.data["priority"] = self.myAgent.priority
				# O agente nao esta usando essa informacao mas OK
				self.data["event_id"] = self.myAgent.event_id
				self.data["group_name"] = self.myAgent.group_name
				#self.data["optional"] = self.myAgent.optional
				#######################################################################IMPORTANTE
				# Recebi todas as mensagem confirmando mas tenho que falar pra eles que todos aceitaram
				#  e nao ha nada na Wiki a respeito

				# Sends the confirmation of event to all participants of the group
				for address in address_list:
		
					receiver = spade.AID.aid(name=address, 
	                                 addresses=["xmpp://" + address])	

					self.msg = spade.ACLMessage.ACLMessage()   
					self.msg.setPerformative("confirm")        
					self.msg.setProtocol("Group")              
					self.msg.setOntology("group_event")		   
					self.msg.setContentObject(self.data)       
					self.msg.addReceiver(receiver)             

					self.myAgent.send(self.msg)		
					print "Send confirmation of event to: " + address

					time.sleep(1)

		else:
			print "Waiting..."

class RemoveGroupEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		print "RemoveGroupEvent"

		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))	
		remove = int(self.myAgent.content.__getattr__("rdf:remove"))	
		
		# Delete the event and extract the name of respective group
		with open(self.myAgent.jid + ".txt", 'r+') as f:
			lines = f.readlines()
			f.seek(0)
			for i, line in enumerate(lines):
				if i != remove: 
					f.write(line)
				else:
					start = "Group: "
					end = " $"
					obj = FindBetween()
					self.myAgent.group_name = obj.find_between(line, start, end)	
					if "$$" in line:
						self.myAgent.optional = "true"
					else:
						self.myAgent.optional = "false"	
			f.truncate()		
			f.close()

		self.myAgent.deregister = self.myAgent.event_id
		self.myAgent.addBehaviour(UnsubscribeServiceDescriptionBehavior())	 
		# Alterei isso. Eu diminui o numero de behaviours necessarios para excluir um evento ja que
		#  no SPADE nao e possivel passar objetos como parametro, logo apenas informo aos agentes
		#  o tipo de evento que deve ser removido e ja efeuto as operacoes de remocao nesse 
		#  proprio comportamento
		self.myAgent.addBehaviour(InformRemoveBehavior())	
		self.myAgent.addBehaviour(SendNotificationToManager())
			
class ModifyGroupEventMessage(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		# Checks if he is the event creator and collision with other events
		obj1 = CreatedEvents()
		obj2 = CheckCollisions()

		self.myAgent.event = str(self.myAgent.content.__getattr__("rdf:event"))
		self.myAgent.day = int(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = int(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = int(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.hour = int(self.myAgent.content.__getattr__("rdf:hour"))
		self.myAgent.duration = int(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))
		self.myAgent.optional = str(self.myAgent.content.__getattr__("rdf:optional"))
		self.myAgent.modify = int(self.myAgent.content.__getattr__("rdf:modify"))

		collision = obj2.check(self.myAgent.jid, self.myAgent.hour, self.myAgent.day,\
		 self.myAgent.month, self.myAgent.year, self.myAgent.duration)	

		if self.myAgent.event_id in obj1.events and collision == False:

			self.myAgent.addBehaviour(ResponseModifyGroupEvent())

			s = self.myAgent.searchService(spade.DF.Service(name=self.myAgent.group_name))	
			self.myAgent.num_usr_group = len(s) - 1
			self.myAgent.pendent_answer	= self.myAgent.num_usr_group
			i = 0
			while i < len(s):
				start = "<ownership>"
				end = "</ownership>"
				stringSubject = str(s[i])
				obj = FindBetween()
				address = obj.find_between(stringSubject, start, end)
				# Invites only others users of group (exclude myself)
				if address != self.myAgent.jid:
					print address + "/" + self.myAgent.jid
					self.data = spade.content.ContentObject()
					self.data["day"] = str(self.myAgent.day)
					self.data["month"] = str(self.myAgent.month)
					self.data["year"] = str(self.myAgent.year)
					self.data["hour"] = str(self.myAgent.hour)
					self.data["duration"] = str(self.myAgent.duration)
					self.data["event"] = str(self.myAgent.event)
					self.data["priority"] = str(self.myAgent.priority)
					self.data["optional"] = str(self.myAgent.optional)
					self.data["event_id"] = str(self.myAgent.event_id)
					self.data["host"] = str(self.myAgent.jid)

					receiver = spade.AID.aid(name=address, 
		                                 addresses=["xmpp://" + address])
					
					self.msg = spade.ACLMessage.ACLMessage()   
					self.msg.setPerformative("propagate")      
					self.msg.setProtocol("Group")              
					self.msg.setOntology("group_event")		   
					self.msg.addReceiver(receiver)             
					self.msg.setContentObject(self.data)       

					self.myAgent.send(self.msg)

				i += 1

class ResponseModifyGroupEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		with open(self.myAgent.jid + ".txt", 'r+') as f:
			lines = f.readlines()
			f.seek(0)
			for i, line in enumerate(lines):
				if i != self.myAgent.modify: 
					f.write(line)
				else:	
					event = self.myAgent.event
					day = str(self.myAgent.day)
					month = str(self.myAgent.month)
					year = str(self.myAgent.year)
					hour = str(self.myAgent.hour)
					duration = str(self.myAgent.duration)
					priority = self.myAgent.priority
					event_id = self.myAgent.event_id
					group_name = self.myAgent.group_name
					optional = self.myAgent.optional
					if optional == "true":
						optional = "$"
					else:
						optional = ""	
						
					f.write("Event: "+event+ " Hour: "+hour+" Day: "+day+" Month: "+month+" Year: "\
						+year+" Duration: "+duration+" Priority: "+priority+" Group: "+group_name+" $"\
						+event_id+"%"+optional+"\n")	
			f.truncate()		
			f.close()

		self.myAgent.addBehaviour(SendNotificationToManager())
			
class ExecuteRemoveOptionalGroupEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))

		s = self.myAgent.searchService(spade.DF.Service(name=self.myAgent.event_id))
		self.myAgent.DEBUG("Found service " + str(s[0]),'ok')	

		if s.len() > 1:
			pass
		else:	
			with open(self.myAgent.jid + ".txt", 'r+') as f:
				lines = f.readlines()
				f.seek(0)
				for line in lines:
					if not self.myAgent.event_id in line:
						f.write(line)
					else:
						pass
				f.truncate()		
				f.close()

			self.myAgent.deregister = self.myAgent.event_id
			self.myAgent.addBehaviour(UnsubscribeServiceDescriptionBehavior())	

class ExecuteRemoveGroupEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))
		print self.myAgent.event_id
		with open(self.myAgent.jid + ".txt", 'r+') as f:
			lines = f.readlines()
			f.seek(0)
			for line in lines:
				if not self.myAgent.event_id in line:
					f.write(line)
				else:
					pass
			f.truncate()		
			f.close()

		self.myAgent.deregister = self.myAgent.event_id
		self.myAgent.addBehaviour(UnsubscribeServiceDescriptionBehavior())	

class ExecuteModifyGroupEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.myAgent.day = str(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = str(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = str(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.hour = str(self.myAgent.content.__getattr__("rdf:hour"))
		self.myAgent.duration = str(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.event = str(self.myAgent.content.__getattr__("rdf:event"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		self.myAgent.optional = str(self.myAgent.content.__getattr__("rdf:optional"))
		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))

		with open(self.myAgent.jid + ".txt", 'r+') as f:			
			lines = f.readlines()
			f.seek(0)
			for line in lines:
				if self.myAgent.event_id not in line: 
					f.write(line)
				else:	
					event = self.myAgent.event
					day = self.myAgent.day
					month = self.myAgent.month
					year = self.myAgent.year
					hour = self.myAgent.hour
					duration = self.myAgent.duration
					priority = self.myAgent.priority
					event_id = self.myAgent.event_id
					group_name = self.myAgent.group_name
					optional = self.myAgent.optional
					# If OPTIONAL, add more one dollar sign
					if optional == "true":
						optional = "$"
					else:
						optional = ""	
						
					f.write("Event: "+event+ " Hour: "+hour+" Day: "+day+" Month: "+month+" Year: "\
						+year+" Duration: "+duration+" Priority: "+priority+" Group: "+group_name+" $"\
						+event_id+"%"+optional+"\n")	
			f.truncate()		
			f.close()

# Isso nao deveria ser apenas InformRemoveOptionalBehavior pq e possivel
#  em tese remover qualquer evento de grupos ate mesmo os mandatarios por isso
#  modifiquei para tratar ambos os casos
class InformRemoveBehavior(spade.Behaviour.OneShotBehaviour):		
	def _process(self):
		s = self.myAgent.searchService(spade.DF.Service(name=self.myAgent.group_name))
		i = 0
		while i < len(s):
			start = "<ownership>"
			end = "</ownership>"
			stringSubject = str(s[i])
			obj = FindBetween()
			address = obj.find_between(stringSubject, start, end)
			# Invites only other users of group (exclude myself)
			if address != self.myAgent.jid:
				#print address + "/" + self.myAgent.jid
				self.data = spade.content.ContentObject()
				self.data["group_name"] = self.myAgent.group_name
				self.data["event_id"] = self.myAgent.event_id
				self.data["optional"] = self.myAgent.optional

				receiver = spade.AID.aid(name=address, 
                                 addresses=["xmpp://" + address])

				self.msg = spade.ACLMessage.ACLMessage()   
				if self.myAgent.optional == "true":
					self.msg.setPerformative("request")    
				else:
					self.msg.setPerformative("disconfirm")	
				self.msg.setProtocol("Group")              
				self.msg.setOntology("group_event")		   
				self.msg.addReceiver(receiver)             
				self.msg.setContentObject(self.data)       

				self.myAgent.send(self.msg)		

			i += 1

class ResponseAddGroupEvent(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.myAgent.day = int(self.myAgent.content.__getattr__("rdf:day"))
		self.myAgent.month = int(self.myAgent.content.__getattr__("rdf:month"))
		self.myAgent.year = int(self.myAgent.content.__getattr__("rdf:year"))
		self.myAgent.hour = int(self.myAgent.content.__getattr__("rdf:hour"))
		self.myAgent.duration = int(self.myAgent.content.__getattr__("rdf:duration"))
		self.myAgent.event = str(self.myAgent.content.__getattr__("rdf:event"))
		self.myAgent.priority = str(self.myAgent.content.__getattr__("rdf:priority"))
		self.myAgent.optional = str(self.myAgent.content.__getattr__("rdf:optional"))
		self.myAgent.event_id = str(self.myAgent.content.__getattr__("rdf:event_id"))
		self.myAgent.group_name = str(self.myAgent.content.__getattr__("rdf:group_name"))

		self.myAgent.addBehaviour(ExecuteAddGroupEvent())

class SendNotificationToManager(spade.Behaviour.OneShotBehaviour):
	def _process(self):
		self.msg = spade.ACLMessage.ACLMessage()     
		self.msg.setPerformative("agree")            
		self.msg.setProtocol("Manager")              
		
		receiver = spade.AID.aid(name= self.myAgent.manager, 
                             addresses=["xmpp://" + self.myAgent.manager])
		self.msg.addReceiver(receiver)               

		self.data = spade.content.ContentObject()
		self.data["name"] = self.myAgent.jid
		self.data["event"] = self.myAgent.event	
		
		self.msg.setContentObject(self.data)    

		self.myAgent.send(self.msg)						
