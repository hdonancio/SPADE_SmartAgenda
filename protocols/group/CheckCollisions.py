class CheckCollisions(object):
	def check(self, user_name, event_hour, event_day, event_month, event_year, event_duration):
		#[P] Change open() to relative diretory
		with open("/home/henrique/Documents/Research/Smart_Agenda_Project/Agent/SPADE_SmartAgenda/"+ user_name + ".txt", 'r+') as f:
			lines = f.readlines()
			if lines == '':
				return False
			for line in lines:
				hour = int(line.split(' ')[3])
				day = int(line.split(' ')[5])
				month = int(line.split(' ')[7])
				year = int(line.split(' ')[9])
				duration = int(line.split(' ')[11])
				if year != event_year:
					pass
				elif month != event_month:
					pass
				elif day != event_day:
					pass
				elif event_hour < hour and event_hour + event_duration <= hour:
					pass
				elif event_hour > hour and event_hour >= hour + duration:
					pass
				else:
					return True	
			return False		