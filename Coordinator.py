import spad
import time
import datetime
from ManagerAgent import ManagerAgent
from protocols.generic.RecvMsgBehav import ReceiveMessageBehaviourMC

class CoordinatorAgentWSGI(spade.Agent.Agent):

	# Number of managers in the system
	global __numOfManagers
	# Number of users in the system
	global __numOfUsers
	# The password to connect to the platform SPADE
	global __password 
	# Managers dict (like a hash table) to find user's manager
	global managers
	managers = {}
	# Max of users per manager
	global __maxUsrMan
	# Number of messages received
	global __numOfMsgs

	def _setup(self):
		print "Coordinator Agent starting . . ."
		global __numOfManagers, __numOfUsers, __maxUsrMan, __password, __numOfMsgs
		__numOfManagers = 0
		__numOfUsers = 0
		__maxUsrMan = 3
		__password = "secret"
		__numOfMsgs = 0

		# Create a first manager 
		__numOfManagers =+ 1
		jid = "manager" + str(__numOfManagers) + "@127.0.0.1"	# The manager identification
		self.createManager(jid, __password)

		# Create the template for the EventBehaviour
		# EventBehaviour only receive (triggered by) messages with this parameters. In this
		#  case, only messages with ontology tocoordinator
		template = spade.Behaviour.ACLTemplate()
		template.setOntology("tocoordinator")
		t = spade.Behaviour.MessageTemplate(template)

		# Add the EventBehaviour with template t
		self.addBehaviour(ReceiveMessageBehaviourMC(), t)		
		
	def createManager(self, jid, password):
		a = ManagerAgent(jid, password)
		a.setIdM(jid)		
		a.setPassword(password)
		a.start()

	# Creates two agents associated an one user. A new one Assistant agent and a new one
	#  Agenda agent
	def newAccountRequest(self, user_data, name, coordinator):
		address = "manager" + self.getLastManager() + "@127.0.0.1"
		self.addUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   # Instantiate the message
		self.msg.setPerformative("request")        # Set the "request" FIPA performative
		self.msg.setProtocol("Manager")            # Set the protocol of the message
		self.msg.addReceiver(receiver)             # Add the message receiver
		self.msg.setContentObject(user_data)       # Set the message content
		
		coordinator.send(self.msg)				   # Send the message

		return self.waitingConfirmation()

	def newEventAdd(self, user_data, name, coordinator):
		# findUser locates the manager of user	
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("Manager")            
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)		

		return self.waitingConfirmation()

	def askAddEvent(self, user_data, name, coordinator):
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("Manager")            
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)		

		return self.waitingConfirmation()

	def viewAgenda(self, user_data, user, coordinator):	
		global __numOfMsgs
		address = self.findUser(user)		

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])


		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("Manager")            
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)	

		# Wait a certain piece of time, incremented by the number of attempts
		# This is useful because the Agenda Agent needs to return the agenda
		#  txt file in a message

		time.sleep(1)
	
		user_name = ''
		attempts = 0
		while (user != user_name and attempts < 3):
			try:
				user_name = str(self.content.__getattr__("rdf:name"))
				__numOfMsgs += 1
			except:
				attempts += 1
				time.sleep(attempts)	
				now = str(datetime.datetime.now())
				print now + "||sleep over"

		agenda = self.content.__getattr__("rdf:agenda")		
		self.content = None

		return agenda

	def removeEvent(self, user_data, name, coordinator):		
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("Manager")            
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)	

		return self.waitingConfirmation()

	def modifyEvent(self, user_data, name, coordinator):		
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("Manager")            
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)	

		return self.waitingConfirmation()

	def removeGroupEvent(self, user_data, name, coordinator):		
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("GroupReq")           
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)	

		return self.waitingConfirmation()

	def modifyGroupEvent(self, user_data, name, coordinator):	
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("GroupReq")           
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)	

		return self.waitingConfirmation()
	
	def groupEvent(self, user_data, name, coordinator):		
		address = self.findUser(name)

		receiver = spade.AID.aid(name= address, 
                                 addresses=["xmpp://" + address])

		self.msg = spade.ACLMessage.ACLMessage()   
		self.msg.setPerformative("request")        
		self.msg.setProtocol("GroupReq")           
		self.msg.addReceiver(receiver)             
		self.msg.setContentObject(user_data)       
		
		coordinator.send(self.msg)		

		return self.waitingConfirmation()

	def findUser(self, usr):
		global managers
		manager = managers.get(usr) + "@127.0.0.1"
		return manager

	def addUser(self, usr):	
		global __numOfUsers, __numOfManagers, __maxUsrMan, __password, managers

		__numOfUsers += 1
		managers[usr] = "manager" + self.getLastManager()

		# If the number of users reached the maximum supported by the managers, 
		#  create a new one manager
		if __numOfManagers * __maxUsrMan == __numOfUsers:
			__numOfManagers += 1
			jid = "manager" + str(__numOfManagers) + "@127.0.0.1"
			self.createManager(jid, __password)

	def waitingConfirmation(self):
		global __numOfMsgs
		obj = ReceiveMessageBehaviourMC()
		last_msg  = obj.getNumOfMsgs()
		attempts = 0
		while(attempts < 4):
			#print "LAST MSG:> "+str(last_msg)+" NUMofMSGS:> "+str(__numOfMsgs)
			last_msg  = obj.getNumOfMsgs()
			if last_msg == __numOfMsgs + 1:
				__numOfMsgs += 1 
				if self.status == "success":
					return True
				else:
					return False	
			else:
				attempts += 1
				time.sleep(attempts)	 	
		return False			

	def getLastManager(self):
		global __numOfManagers
		return str(__numOfManagers)