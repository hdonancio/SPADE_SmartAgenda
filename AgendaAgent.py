import spade
import protocols.generic.RecvMsgBehav
from protocols.generic.RegisterBehaviour import RegisterBehaviour
from protocols.generic.SubscribeServiceDescriptionBehaviour import SubscribeServiceDescriptionBehaviour

class AgendaAgent(spade.Agent.Agent):		
	def setNameUser(self, jid):		
		self.jid = jid
		self.register = jid
		self.role = "R2"		

	def setManagerJID(self, managerJID):
		self.manager = managerJID	

	def setPreferredHour(self, hour_1, hour_2, hour_3, hour_4, hour_5):
		self.hour_1 = hour_1	
		self.hour_2 = hour_2	
		self.hour_3 = hour_3	
		self.hour_4 = hour_4	
		self.hour_5 = hour_5	

	def _setup(self):
		print "A new Agenda Agent starting . . ."
		b1 = self.import_recvmsgM()
		self.addBehaviour(RegisterBehaviour())
		template1 = spade.Behaviour.ACLTemplate()
		template1.setSender(spade.AID.aid(self.manager,["xmpp://" + self.manager]))
		t1 = spade.Behaviour.MessageTemplate(template1)
		self.addBehaviour(b1, t1)				

		b2 = self.import_recvmsgA()
		template2 = spade.Behaviour.ACLTemplate()
		template2.setOntology("group_event")
		t2 = spade.Behaviour.MessageTemplate(template2)
		self.addBehaviour(b2, t2)

		b3 = self.import_recvmsgMy()
		template3 = spade.Behaviour.ACLTemplate()
		template3.setSender(spade.AID.aid(self.jid,["xmpp://" + self.jid]))
		t3 = spade.Behaviour.MessageTemplate(template3)
		self.addBehaviour(b3, t3)				

		self.addBehaviour(SubscribeServiceDescriptionBehaviour())

		#Open a file to writing events and create it if id does not exist
		f = open(self.jid + ".txt","w+")

	# Because the creation of an agent is triggered by ReceiveMessageBehaviour, Python accuses 
	#  circular dependency. One of way to fix it, is put all classes in a single module or import
	#  a class when this is necessary, how the above code make
	def import_recvmsgM(self):
		return protocols.generic.RecvMsgBehav.ReceiveMessageBehaviourM()

	def import_recvmsgA(self):
		return protocols.generic.RecvMsgBehav.ReceiveMessageBehaviourA()

	def import_recvmsgMy(self):
		return protocols.generic.RecvMsgBehav.ReceiveMessageBehaviourMy()		