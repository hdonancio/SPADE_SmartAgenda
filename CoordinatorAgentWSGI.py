import os
import redis
import spade
from Coordinator import CoordinatorAgentWSGI
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.utils import redirect
from jinja2 import Environment, FileSystemLoader

class WSGI(object):
	def __init__(self, config):
		global template_path
		self.redis = redis.Redis(config['redis_host'], config['redis_port'])
		template_path = os.path.join(os.path.dirname(__file__), 'templates')
		self.jinja_env = Environment(loader=FileSystemLoader(template_path),
		                             autoescape=True)
		self.url_map = Map([
		    Rule('/', endpoint='layout')
		    #Rule('/', endpoint='new_account'),
		    #Rule('/', endpoint='layout')
		])
		self.start_agent()
		
	def render_template(self, template_name, **context):
		t = self.jinja_env.get_template(template_name)
		return Response(t.render(context), mimetype='text/html')

	def dispatch_request(self, request):
	    adapter = self.url_map.bind_to_environ(request.environ)
	    try:
	        endpoint, values = adapter.match()
	        return getattr(self, 'on_' + endpoint)(request, **values)
	    except HTTPException, e:
	        return e
    
	def wsgi_app(self, environ, start_response):
		request = Request(environ)
		response = self.dispatch_request(request)
		return response(environ, start_response)

	def __call__(self, environ, start_response):
		return self.wsgi_app(environ, start_response)

	def start_agent(self):	
		global coordinator
		coordinator = CoordinatorAgentWSGI("coordinator@127.0.0.1", "secret")
		coordinator.start()

########################################################################################
########################################################################################
	
	def on_layout(self, request):
		self.render_template('layout.html')
		global coordinator
		global user_name
		global id_
		global event_id
		error = None
		name = ''
		event = ''
		print request.form.values()
		if request.method == 'POST':
			# Create a new account
			if request.form.has_key('firstname'):
				name = request.form['firstname']
				if name[0].isupper():					
					transport1 = request.form['transport1']	
					transport2 = request.form['transport2']	
					transport3 = request.form['transport3']	
					hotel = request.form['hotel']
					hour_1 = request.form['hour_1']
					hour_2 = request.form['hour_2']
					hour_3 = request.form['hour_3']
					hour_4 = request.form['hour_4']
					hour_5 = request.form['hour_5']
					user_data = spade.content.ContentObject()
					user_data["name"] = name
					user_data["transport1"] = transport1
					user_data["transport2"] = transport2
					user_data["transport3"] = transport3
					user_data["hotel"] = hotel
					user_data["hour_1"] = hour_1
					user_data["hour_2"] = hour_2
					user_data["hour_3"] = hour_3
					user_data["hour_4"] = hour_4
					user_data["hour_5"] = hour_5
					status = coordinator.newAccountRequest(user_data, name, coordinator)				
					if status == True:
						return self.render_template('new_account.html')				
					else:
						return self.render_template('layout.html') 	
			#Create a new event			
			elif request.form.has_key('event'):
				user = request.form['user']	 
				day = request.form['day']	
				month = request.form['month']
				year = request.form['year']
				hour = request.form['hour']
				duration = request.form['duration']
				priority = request.form['priority']
				event = request.form['event']
				user_data = spade.content.ContentObject()
				user_data["user"] = user			
				user_data["day"] = day
				user_data["month"] = month
				user_data["year"] = year
				user_data["hour"] = hour
				user_data["duration"] = duration
				user_data["priority"] = priority
				user_data["event"] = event
				if request.form.has_key('movable'):
					user_data["movable"] = "true"
				else: 
					user_data["movable"] = "false"	
				if request.form['group_event'] != "":
					group_event = request.form['group_event']	
					user_data["group_event"] = group_event
					user_data["user_group"] = user
					if request.form.has_key('optional'):
						user_data["optional"] = "true"
					status = coordinator.groupEvent(user_data, user, coordinator)													 	
					if status == True:
						return self.render_template('new_event.html')				
					elif status == False:
						return self.render_template('layout.html')
				else:
					if request.form['hour'] != "": 
						status = coordinator.newEventAdd(user_data, user, coordinator)		
					elif request.form['hour'] == "":	
						#if request.form['event'] == "":
							#user_data["event"] = "Random Event"
						status = coordinator.askAddEvent(user_data, user, coordinator)
					if status == True:
						return self.render_template('new_event.html')				
					elif status == False:
						return self.render_template('layout.html')
			# View agenda			
			elif request.form.has_key('user_name'):
				user_name = request.form['user_name']	 
				user_data = spade.content.ContentObject()
				user_data["user_name"] = user_name
				agenda = coordinator.viewAgenda(user_data, user_name, coordinator)																
				self.viewAgenda(agenda)
				return self.render_template('view_agenda.html')				
			# Remove an event	
			elif "remove" in request.form.values():
				keys = request.form.keys()
				key = keys[0]
				id_ = key.partition('.')[2]
				user_data = spade.content.ContentObject()
				user_data["user_name"] = user_name
				user_data["remove"] = id_
				if request.form.has_key('gr_rm_md'):
					user_data["user_group"] = user_name
					#user_data["group"] = ""
					user_data["event_id"] = str(request.form.get('gr_rm_md'))
					status = coordinator.removeGroupEvent(user_data, user_name, coordinator)
				else:
					status = coordinator.removeEvent(user_data, user_name, coordinator)
				if status == True:
						return self.render_template('op_success.html')				
				elif status == False:
					return self.render_template('op_failed.html')														
			# Modify an event		
			elif "modify" in request.form.values():
				keys = request.form.keys()
				key = keys[0]
				id_ = key.partition('.')[2]
				if request.form.has_key('gr_rm_md'):					
					event_id = str(request.form.get('gr_rm_md'))
					return self.render_template('modify_gr.html')
				return self.render_template('modify.html')				
			elif request.form.has_key('new_event'):	
				day = request.form['new_day']	
				month = request.form['new_month']
				year = request.form['new_year']
				hour = request.form['new_hour']
				event = request.form['new_event']
				duration = request.form['duration']
				priority = request.form['priority']
				user_data = spade.content.ContentObject()
				user_data["usr_name"] = user_name
				user_data["modify"] = id_
				user_data["new_day"] = day
				user_data["new_month"] = month
				user_data["new_year"] = year
				user_data["new_hour"] = hour
				user_data["new_event"] = event
				user_data["new_duration"] = duration
				user_data["new_priority"] = priority
				if request.form.has_key('movable'):
					user_data["movable"] = "true"
				else: user_data["movable"] = "false"	
				status = coordinator.modifyEvent(user_data, user_name, coordinator)															
				if status == True:
						return self.render_template('op_success.html')				
				elif status == False:
					return self.render_template('op_failed.html')														
			# Modify a group event		
			elif request.form.has_key('new_event_gr'):		
				day = request.form['new_day_gr']	
				month = request.form['new_month_gr']
				year = request.form['new_year_gr']
				hour = request.form['new_hour_gr']
				event = request.form['new_event_gr']
				duration = request.form['duration']
				priority = request.form['priority']
				user_data = spade.content.ContentObject()
				user_data["modify"] = id_
				user_data["new_day"] = day
				user_data["new_month"] = month
				user_data["new_year"] = year
				user_data["new_hour"] = hour
				user_data["new_event"] = event
				user_data["new_duration"] = duration
				user_data["new_priority"] = priority
				user_data["user_group"] = user_name
				user_data["event_id"] = event_id
				if request.form.has_key('optional'):
					user_data["optional"] = "true"
				else: user_data["optional"] = "false"	
				status = coordinator.modifyGroupEvent(user_data, user_name, coordinator)
				if status == True:
						return self.render_template('op_success.html')				
				elif status == False:
					return self.render_template('op_failed.html')
			# Create, subscribe and leave groups		
			elif request.form.has_key('your_name'):
				group = request.form['group_name']	
				user_group = request.form['your_name']
				user_data = spade.content.ContentObject()
				user_data["group"] = group
				user_data["user_group"] = user_group
				if "Create a group" in request.form.values():
					user_data["create"] = "true"
				elif "Subscribe to a group" in request.form.values():
					user_data["subscribe"] = "true"
				elif "Leave a group" in request.form.values():
					user_data["leave"] = "true"
				status = coordinator.groupEvent(user_data, user_group, coordinator)	
				if status == True:
						return self.render_template('op_success.html')				
				elif status == False:
					return self.render_template('op_failed.html')
		return self.render_template('layout.html')

########################################################################################
########################################################################################

	def viewAgenda(self, agenda):
		count = 0
		content = ""
		for line in agenda.splitlines():
			if line != "\n" and "$" not in line:
				form = "<form action=\"\" method = \"post\" style=display:inline-block;\"> <input type=\"submit\" value =\"remove\" name = \"remove."+str(count)+"\">" \
				"</form> <form action=\"\" method = \"post\" style=display:inline-block;\"><input type=\"submit\" value =\"modify\" name = \"modify."+str(count)+"\">" \
				"</form>\n<br><br>\n"
			elif line != "\n" and "$" in line:
				start = "$"
				end = "%"
				event_id = self.find_between(line, start, end)
				form = "<form action=\"\" method = \"post\" style=display:inline-block;\"> <input type=\"submit\" value =\"remove\" name = \"remove."+str(count)+"\">" \
				"<input type=\"hidden\" name=\"gr_rm_md\" value=\""+event_id+"\"></form>"+\
				"<form action=\"\" method = \"post\" style=display:inline-block;\"><input type=\"submit\" value =\"modify\" name = \"modify."+str(count)+"\">" \
				"<input type=\"hidden\" name=\"gr_rm_md\" value=\""+event_id+"\"></form>\n<br><br>\n"
			content = content + (line + form) 	
			count = count + 1	
		global template_path	
		rel_path = "/layout.html"
		abs_file_path = template_path + rel_path
		w = False
		e = False
		print abs_file_path
		with open(abs_file_path) as fin, open("templates/view_agenda.html","w+") as fout:
		    for line in fin:
				if not e:
					fout.write(line)	
				if w:
					fout.write(content)
					e = True
					w = False
				if "<!--BEGIN-->" in line:
					w = True
				if "<!--END-->" in line:
					e = False
					fout.write("<!--END-->\n")
		fout.close()
		fin.close()		   

	def find_between(self, s, first, last ):
		try:
		    start = s.index( first ) + len( first )
		    end = s.index( last, start )
		    #print s[start:end]
		    return s[start:end]
		except ValueError:
		    return ""

########################################################################################
########################################################################################

def create_app(redis_host='localhost', redis_port=6379, with_static=True):
    app = WSGI({
        'redis_host':       redis_host,
        'redis_port':       redis_port
    })
    if with_static:
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/static':  os.path.join(os.path.dirname(__file__), 'static')
        })
    return app

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app()
    run_simple('127.0.0.1', 5000, app, use_debugger=True, use_reloader=False)    
