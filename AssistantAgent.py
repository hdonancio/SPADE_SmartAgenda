import spade
import protocols.generic.RecvMsgBehav
from protocols.generic.RegisterBehaviour import RegisterBehaviour
from protocols.generic.SubscribeServiceDescriptionBehaviour import SubscribeServiceDescriptionBehaviour

class AssistantAgent(spade.Agent.Agent):		
	def setNameUser(self, jid):		
		self.jid = jid
		self.register = jid
		self.role = "R3"		

	def setManagerJID(self, managerJID):
		self.manager = managerJID	

	def _setup(self):
		print "A new Assistant Agent starting . . ."
		b = self.import_recvmsg()
		self.addBehaviour(RegisterBehaviour())
		# Create the template for the EventBehaviour
		template = spade.Behaviour.ACLTemplate()
		template.setSender(spade.AID.aid(self.jid,["xmpp://" + self.manager]))
		t = spade.Behaviour.MessageTemplate(template)		
		self.addBehaviour(b, t)		

		self.addBehaviour(SubscribeServiceDescriptionBehaviour())

	# Because the creation of an agent is triggered by ReceiveMessageBehaviour, Python accuses 
	#  circular dependency. One of way to fix it, is put all classes in a single module or import
	#  a class when this is necessary, how the above code make
	def import_recvmsg(self):
		return protocols.generic.RecvMsgBehav.ReceiveMessageBehaviourM()
			
