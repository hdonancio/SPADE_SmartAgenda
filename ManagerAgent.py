import spade
from protocols.generic.RegisterBehaviour import RegisterBehaviour
from protocols.generic.RecvMsgBehav import ReceiveMessageBehaviourC
from protocols.generic.RecvMsgBehav import ReceiveMessageBehaviourM
from protocols.generic.RecvMsgBehav import ReceiveMessageBehaviourAA
from protocols.generic.RecvMsgBehav import ReceiveMessageBehaviourAF
from protocols.generic.SubscribeServiceDescriptionBehaviour import SubscribeServiceDescriptionBehaviour

class ManagerAgent(spade.Agent.Agent):		

	def setIdM(self, jid):		
		self.jid = jid
		self.register = jid
		self.role = "R1"		

	def setPassword(self, password):
		self.password = password	

	def _setup(self):
		print "A new Manager Agent starting . . ."

		self.addBehaviour(RegisterBehaviour())

		template = spade.Behaviour.ACLTemplate()
		template.setSender(spade.AID.aid("coordinator@127.0.0.1",["xmpp://coordinator@127.0.0.1"]))
		t1 = spade.Behaviour.MessageTemplate(template)
		self.addBehaviour(ReceiveMessageBehaviourC(), t1)		
		

		template = spade.Behaviour.ACLTemplate()
		template.setSender(spade.AID.aid(self.jid,["xmpp://" + self.jid]))
		t2 = spade.Behaviour.MessageTemplate(template)
		self.addBehaviour(ReceiveMessageBehaviourM(), t2)		

		template = spade.Behaviour.ACLTemplate()
		template.setPerformative("agree")
		t3 = spade.Behaviour.MessageTemplate(template)
		self.addBehaviour(ReceiveMessageBehaviourAA(), t3)		

		template = spade.Behaviour.ACLTemplate()
		template.setPerformative("failure")
		t4 = spade.Behaviour.MessageTemplate(template)
		self.addBehaviour(ReceiveMessageBehaviourAF(), t4)		

		self.addBehaviour(SubscribeServiceDescriptionBehaviour())